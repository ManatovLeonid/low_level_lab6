#include <stdint.h>
#include <stdio.h>
#include <malloc.h>
#include <math.h>
#include "images.h"

uint8_t cut_chanel(uint64_t x) {
    if (x < 256) return x;
    return 255;
}

uint8_t sum_overflow_chanel(uint8_t a, uint8_t b) {
    uint8_t c = a + b;
    if (c < a || c < b)c = 255;
    return c;
}

struct image create_image(uint32_t width, uint32_t height) {
    struct image img;
    img.width = width;
    img.height = height;
    img.pixels = malloc(img.height * img.width * sizeof(struct pixel));
    return img;
}

struct pixel *create_pixel(uint8_t b, uint8_t g, uint8_t r) {

    struct pixel *new_pixel = malloc(sizeof(struct pixel));
    new_pixel->b = cut_chanel(b);
    new_pixel->g = cut_chanel(g);
    new_pixel->r = cut_chanel(r);
    return new_pixel;
}

