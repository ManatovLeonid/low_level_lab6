
#ifndef BMPROTATER_IMAGES_H
#define BMPROTATER_IMAGES_H

#include <stdint.h>
#include <stdio.h>


struct __attribute__((packed)) pixel {
    uint8_t b, g, r;
};
struct __attribute__((packed)) image {
    uint32_t width, height;
    struct pixel *pixels;
};


struct image create_image(uint32_t width, uint32_t height);

struct pixel *create_pixel(uint8_t b, uint8_t g, uint8_t r);

uint8_t cut_chanel(uint64_t x);
uint8_t sum_overflow_chanel(uint8_t a, uint8_t b);
#endif //BMPROTATER_IMAGES_H
