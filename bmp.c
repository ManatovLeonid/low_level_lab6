#include <stdint.h>
#include <stdio.h>
#include <malloc.h>
#include <math.h>
#include "images.h"
#include "bmp.h"



void bmp_header_print(struct bmp_header header) {
    printf("bfType: %#x\n", header.bfType);
    printf("bfileSize: %u\n", header.bfileSize);
    printf("bfReserved: %u\n", header.bfReserved);
    printf("bOffBits: %u\n", header.bOffBits);
    printf("biSize: %u\n", header.biSize);
    printf("biWidth: %u\n", header.biWidth);
    printf("biHeight: %u\n", header.biHeight);
    printf("biPlanes: %u\n", header.biPlanes);
    printf("biBitCount: %u\n", header.biBitCount);
    printf("biCompression: %u\n", header.biCompression);
    printf("biSizeImage: %u\n", header.biSizeImage);
    printf("biXPelsPerMeter: %u\n", header.biXPelsPerMeter);
    printf("biYPelsPerMeter: %u\n", header.biYPelsPerMeter);
    printf("biClrUsed: %u\n", header.biClrUsed);
    printf("biClrImportant: %u\n", header.biClrImportant);
}


struct bmp_header create_header(struct image *img) {
    struct bmp_header header;
    uint64_t remainder;

    header.bfType = 0x4d42;
    header.bfReserved = 0;
    header.bOffBits = 54;
    header.biSize = 40;
    header.biPlanes = 1;
    header.biBitCount = 24;
    header.biCompression = 0;
    header.biXPelsPerMeter = 0;
    header.biYPelsPerMeter = 0;
    header.biClrUsed = 0;
    header.biClrImportant = 0;

    header.biHeight = img->height;
    header.biWidth = img->width;

    remainder = (img->width * 3l) % 4l;
    remainder = (remainder == 0) ? 0 : (4l - remainder);
    header.biSizeImage = (img->width * 3l + remainder) * img->height;
    header.bfileSize = header.biSizeImage + header.bOffBits;

    return header;
}


enum read_status from_bmp(FILE *in_bmp, struct image *in_image) {

    struct bmp_header header;

    if (!fread(&header, sizeof(header), 1, in_bmp)) return READ_IO_ERROR;

//    bmp_header_print(header);
    uint8_t spare[4];
    int64_t remainder, row;

    *in_image = create_image(header.biWidth, header.biHeight);


    /* how many bytes to skip to get on the next row */
    remainder = (in_image->width * 3l) % 4l;
    remainder = (remainder == 0) ? 0 : (4l - remainder);

    for (row = in_image->height - 1; row >= 0; row--) {

        uint64_t row_bits = fread(&in_image->pixels[row * in_image->width], sizeof(struct pixel), in_image->width,
                                  in_bmp);
        uint64_t rem_bits = fread(spare, sizeof(uint8_t), remainder, in_bmp);

        if (!row_bits || (remainder && !rem_bits)) {
            free(in_image->pixels);
            return READ_IO_ERROR;
        }

    }


    return READ_OK;
}


enum write_status to_bmp(FILE *out_bmp, struct image *out_image) {
    int64_t remainder, row;
    const uint8_t spare[4] = {0};
    struct bmp_header header;

    header = create_header(out_image);
    if (!fwrite(&header, sizeof(header), 1, out_bmp)) return WRITE_IO_ERROR;

    /* how many bytes to skip to get on the next row */
    remainder = (out_image->width * 3l) % 4l;
    remainder = (remainder == 0) ? 0 : (4l - remainder);

    for (row = out_image->height - 1; row >= 0; row--) {
        uint64_t row_bits = fwrite(&out_image->pixels[row * out_image->width], sizeof(struct pixel), out_image->width,
                                   out_bmp);
        uint64_t rem_bits = fwrite(spare, sizeof(uint8_t), remainder, out_bmp);
        if (!row_bits || (remainder && !rem_bits)) return WRITE_IO_ERROR;
    }

    return WRITE_OK;
}



void print_error_read(enum read_status status) {
    if (status == READ_OK) return;
    char *err_msg;

    switch (status) {

        case READ_INVALID_SIGNATURE:
            err_msg = "Invalid signature";
            break;
        case READ_INVALID_BITS:
            err_msg = "Invalid bit count";
            break;
        case READ_INVALID_HEADER:
            err_msg = "Invalid header";
            break;
        case READ_IO_ERROR:
            err_msg = "Read IO error";
            break;
        default:
            err_msg = "Read error";
            break;
    }

    fprintf(stderr, "%s\n", err_msg);
}


void print_error_write(enum write_status status) {
    if (status == WRITE_OK) return;
    char *err_msg;

    switch (status) {
        case WRITE_IO_ERROR:
            err_msg = "Write IO error";
            break;
        default:
            err_msg = "Write error";
            break;
    }

    fprintf(stderr, "%s\n", err_msg);
}

