#ifndef _BMP_H_
#define _BMP_H_

#include <stdint.h>
#include <stdio.h>

struct __attribute__((packed)) bmp_header {
    uint16_t bfType;
    uint32_t bfileSize;
    uint32_t bfReserved;
    uint32_t bOffBits;
    uint32_t biSize;
    uint32_t biWidth;
    uint32_t biHeight;
    uint16_t biPlanes;
    uint16_t biBitCount;
    uint32_t biCompression;
    uint32_t biSizeImage;
    uint32_t biXPelsPerMeter;
    uint32_t biYPelsPerMeter;
    uint32_t biClrUsed;
    uint32_t biClrImportant;
};

enum read_status {
    READ_OK = 0,
    READ_INVALID_SIGNATURE,
    READ_INVALID_BITS,
    READ_INVALID_HEADER,
    READ_IO_ERROR
/* more codes */
};

enum write_status {
    WRITE_OK = 0,
    WRITE_IO_ERROR
/* more codes */
};


enum read_status from_bmp(FILE *in_bmp, struct image *in_image);

enum write_status to_bmp(FILE *out_bmp, struct image *out_image);


struct bmp_header create_header(struct image *img);


void print_error_read(enum read_status status);

void print_error_write(enum write_status status);

#endif