#ifndef BMPROTATER_IMAGETRANSFORMATIONS_H
#define BMPROTATER_IMAGETRANSFORMATIONS_H

/* makes a rotated copy */
struct image rotate90(struct image  *source);

struct image sepia_sse(struct image * const source);

struct image sepia(struct image * const source);

struct image blur_image(struct image *image);

struct image dilate_image(struct image *image);

struct image erode_image(struct image *image);

void packed_mul(float *result, float const *mul1, float const *mul2);


#endif //BMPROTATER_IMAGETRANSFORMATIONS_H
