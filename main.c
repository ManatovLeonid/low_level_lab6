#include <stdio.h>
#include <stdint.h>
#include <inttypes.h>
#include <getopt.h>
#include <stdlib.h>
#include "images.h"
#include "imageTransformations.h"
#include "bmp.h"

char *input_filename = "./input.bmp";
char *output_filename = "./output.bmp";


void print_image(struct image *image) {
    printf("Image width: %" PRIu32 "\n", image->width);
    printf("Image height: %" PRIu32 "\n", image->height);


    for (int row = 0; row < image->height; ++row) {
        for (int p = 0; p < image->width; ++p) {

            printf("pixel:\n r%" PRIu8, image->pixels[row * image->height + p].r);
            printf("g%" PRIu8, image->pixels[row * image->height + p].g);
            printf("b%" PRIu8 "\n", image->pixels[row * image->height + p].b);
        }
    }
}

static void do_arg_image_action(struct image *image, int count, struct image (*action)(struct image *image)) {
    for (int i = 0; i < count; ++i)
        *image = action(image);
}


int main(int argc, char *argv[]) {
    int rez = 0;


    FILE *inBmp = fopen(input_filename, "rb");
    FILE *outBmp = fopen(output_filename, "wb+");
    struct image image;
    enum read_status read_status = from_bmp(inBmp, &image);
    if (read_status != READ_OK) print_error_read(read_status);

//	opterr=0;
    while ((rez = getopt(argc, argv, "b:e:d:r:s:")) != -1) {
        switch (rez) {
            case 'b':
                printf("found argument \"blur_count %i times\".\n", atoi(optarg));
                do_arg_image_action(&image, atoi(optarg), blur_image);
                break;
            case 'e':
                printf("found argument \"erode_count %i times\".\n", atoi(optarg));
                do_arg_image_action(&image, atoi(optarg), erode_image);
                break;
            case 'd':
                printf("found argument \"dilate_count %i times\".\n", atoi(optarg));
                do_arg_image_action(&image, atoi(optarg), dilate_image);
                break;
            case 'r':
                printf("found argument \"rotate_count %i times\".\n", atoi(optarg));
                do_arg_image_action(&image, atoi(optarg), rotate90);
                break;
            case 's':
                printf("found argument \"sepia_count %i times\".\n", atoi(optarg));
                do_arg_image_action(&image, atoi(optarg), sepia);
                break;
            case '?':
                fprintf(stderr, "Error found !\n");
                break;
        }
    }


    enum write_status write_status = to_bmp(outBmp, &image);
    if (write_status != WRITE_OK) print_error_write(write_status);


    return 0;
}
